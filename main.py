from __future__ import print_function
from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QHBoxLayout, QVBoxLayout, QLineEdit, QSpinBox, QPushButton, QFileDialog
from PyQt5.QtGui import QPixmap
import sys
import time
import copy
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np
import uvc
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import calibration

### Variables initiales
dev_list = uvc.device_list()
anglesH_calib = np.array([0, 0, 16, 0, -16])
anglesV_calib = np.array([0, 16, 0, -16, 0])
current_angleH = anglesH_calib[0]
current_angleV = anglesV_calib[0]
###

def bubble_sort_lists(li_origin):
    """
    Sort the list of lists by length of its list elements.

    Parameters
    ----------
    li_origin : List of lists

    Returns
    -------
    list_sorted : The list sorted by the size of each of its elements. The
    lists with biggest lengths are firsts. Delete the contours of two pixels or
    less.

    """
    li = copy.deepcopy(li_origin)
    # if li == []:
    #     return []
    for i in range(len(li) - 1, 0, -1):
        for j in range(i):
            if len(li[j]) < len(li[j + 1]):
                temp = li[j + 1]
                li[j + 1] = li[j]
                li[j] = temp
                # li[j + 1], li[j] = li[j], li[j + 1]
    try:
        while len(li[len(li)-1]) <= 2:
            li.pop()
        return li
    except:
        # print(li)
        # import pdb; pdb.set_trace()
        pass

def centroid_contour(c):
    """
    Compute the (non-weighted) centroid of a contour.

    Parameters
    ----------
    c : Open-cv contour.

    Returns
    -------
    cX : X-value of the centroid
    cY : Y-value of the centroid

    """
    M = cv2.moments(c)
    if M["m00"]:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
    else:
        print("Unable to compute the centroid with moments for the\
                        angle of " + ". It is then computed\
                        with a mean of he X-values and Y-values.")
        cX = np.mean(c[1, :, 0])
        cY = np.mean(c[1, :, 1])
    return cX, cY


class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def run(self):
        # capture from web cam
        cap = uvc.Capture(dev_list[0]["uid"])
        # cap.frame_mode = (400, 400, 120)
        cap.frame_mode = (400, 400, 60)

        while True:
            frame = cap.get_frame_robust()
            self.change_pixmap_signal.emit(frame.gray)


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Eye camera")
        self.display_width = 640
        self.display_height = 480
        self.save_im = False
        self.image_save_name = 'image'
        self.nb_frame = 0
        self.thresh = 20
        # Calibration variables
        self.calib_done = False
        self.calib_step = 0
        self.current_angleH = anglesH_calib[self.calib_step]
        self.current_angleV = anglesV_calib[self.calib_step]
        # Tables angles vs centroids positions
        self.calibH = np.zeros((2, anglesH_calib.shape[0])) # Table with the two spot's x-values got for each horizontal calibration angle
        self.calibV = np.zeros((2, anglesV_calib.shape[0])) # Table with the two spot's y-values got for each vertical calibration angle
        # Regression and coefficients
        self.reg_leftH = LinearRegression(fit_intercept=True, normalize=True)
        self.reg_rightH = LinearRegression(fit_intercept=True, normalize=True)
        self.reg_leftV = LinearRegression(fit_intercept=True, normalize=True)
        self.reg_rightV = LinearRegression(fit_intercept=True, normalize=True)
        self.a_leftH = 0
        self.b_leftH = 0
        self.a_rightH = 0
        self.b_rightH = 0
        self.a_leftV = 0
        self.b_leftV = 0
        self.a_rightV = 0
        self.b_rightV = 0
        # Angle which will be computed after calibration
        self.angle_leftH = 0
        self.angle_rightH = 0
        self.angle_leftV = 0
        self.angle_rightV = 0
        # Widget containing the viewer and the calibration box
        # create the label that holds the image
        self.image_label = QLabel(self)
        self.image_label.resize(self.display_width, self.display_height)
        # create a text label
        self.textLabel = QLabel('Left eye')
        # create a widget to set the threshold of the boundaries detection
        self.threshold_widget = QHBoxLayout()
        self.threshold_widget.addWidget(QLabel('Threshold boundaries detection :'))
        self.threshold_spinbox = QSpinBox()
        self.threshold_spinbox.setMinimum(0)
        self.threshold_spinbox.setMaximum(255)
        self.threshold_spinbox.setSingleStep(1)
        self.threshold_spinbox.setValue(self.thresh)
        self.threshold_spinbox.valueChanged.connect(self.value_thresh_changed)
        # self.threshold_spinbox.textChanged.connect(self.value_thresh_changed_str)
        self.threshold_widget.addWidget(self.threshold_spinbox)
        # create a widget for the calibration
        self.calib_widget = QVBoxLayout()
        self.calib_angles_names = QHBoxLayout()
        self.calib_angles_names.addWidget(QLabel('Horizontal angle'))
        self.calib_angles_names.addWidget(QLabel('Vertical angle'))
        self.calib_angles_values = QHBoxLayout()
        self.current_angleH_label = QLabel(str(current_angleH))
        self.current_angleV_label = QLabel(str(current_angleV))
        self.calib_angles_values.addWidget(self.current_angleH_label)
        self.calib_angles_values.addWidget(self.current_angleV_label)
        self.calib_button = QPushButton('Calibration')
        self.calib_button.clicked.connect(self.calib_button_pushed)
        self.calib_widget.addLayout(self.calib_angles_names)
        self.calib_widget.addLayout(self.calib_angles_values)
        self.calib_widget.addWidget(QLabel('Step of calibration'))
        self.calib_step_label = QLabel('Not done')
        self.calib_widget.addWidget(self.calib_step_label)
        self.calib_widget.addWidget(self.calib_button)
        self.calib_widget.addStretch(1)

        self.calib_box = QHBoxLayout()
        self.calib_box.addWidget(self.image_label)
        self.calib_box.addLayout(self.calib_widget)
        self.calib_box.addStretch(1)

        self.imageSaveName = QLineEdit()
        self.imageSaveName.setText(self.image_save_name)
        self.imageSaveName.textChanged.connect(self.image_save_name_changed)
        self.saveButton = QPushButton('Save Image')
        self.saveButton.clicked.connect(self.save_button_pushed)

        # Widget indiquant les barycentres calculés
        self.centroids = np.zeros((2,2))  # Initialisation des valeurs de barycentre
        self.centroid_left_1 = str(self.centroids[0][0])
        self.centroid_left_2 = str(self.centroids[0][1])
        self.centroid_right_1 = str(self.centroids[1][0])
        self.centroid_right_2 = str(self.centroids[1][1])
        self.centroid_left_text_1 = QLabel('Centroid Left')
        self.centroid_left_text_2 = QLabel(self.centroid_left_1)
        self.centroid_left_text_3 = QLabel(self.centroid_left_2)
        self.centroid_left_box = QVBoxLayout()
        self.centroid_left_box.addWidget(self.centroid_left_text_1)
        self.centroid_left_box.addWidget(self.centroid_left_text_2)
        self.centroid_left_box.addWidget(self.centroid_left_text_3)
        self.centroid_right_text_1 = QLabel('Centroid right')
        self.centroid_right_text_2 = QLabel(self.centroid_right_1)
        self.centroid_right_text_3 = QLabel(self.centroid_right_2)
        self.centroid_right_box = QVBoxLayout()
        self.centroid_right_box.addWidget(self.centroid_right_text_1)
        self.centroid_right_box.addWidget(self.centroid_right_text_2)
        self.centroid_right_box.addWidget(self.centroid_right_text_3)
        self.centroid_box = QHBoxLayout()
        self.centroid_box.addLayout(self.centroid_left_box)
        self.centroid_box.addLayout(self.centroid_right_box)

        # Widget indiquant les angles calculés après calibration
        self.angles = np.zeros((2,2))  # Initialisation des valeurs de barycentre
        self.angle_left_1 = str(self.angles[0][0])
        self.angle_left_2 = str(self.angles[0][1])
        self.angle_right_1 = str(self.angles[1][0])
        self.angle_right_2 = str(self.angles[1][1])
        self.angle_left_text_1 = QLabel('Angle Left')
        self.angle_left_text_2 = QLabel(self.angle_left_1)
        self.angle_left_text_3 = QLabel(self.angle_left_2)
        self.angle_left_box = QVBoxLayout()
        self.angle_left_box.addWidget(self.angle_left_text_1)
        self.angle_left_box.addWidget(self.angle_left_text_2)
        self.angle_left_box.addWidget(self.angle_left_text_3)
        self.angle_right_text_1 = QLabel('Angle right')
        self.angle_right_text_2 = QLabel(self.angle_right_1)
        self.angle_right_text_3 = QLabel(self.angle_right_2)
        self.angle_right_box = QVBoxLayout()
        self.angle_right_box.addWidget(self.angle_right_text_1)
        self.angle_right_box.addWidget(self.angle_right_text_2)
        self.angle_right_box.addWidget(self.angle_right_text_3)
        self.angle_box = QHBoxLayout()
        self.angle_box.addLayout(self.angle_left_box)
        self.angle_box.addLayout(self.angle_right_box)

        # Widget indiquant la fréquence de fonctionnement en temps réel
        self.working_freq = 0
        self.freq_label = QLabel(str(self.working_freq))
        self.freq_box = QHBoxLayout()
        self.freq_box.addWidget(QLabel('Frequency'))
        self.freq_box.addWidget(self.freq_label)

        # create a vertical box layout to gather everything in one window
        vbox = QVBoxLayout()
        vbox.addLayout(self.calib_box)
        vbox.addWidget(self.textLabel)
        vbox.addLayout(self.threshold_widget)
        vbox.addWidget(self.imageSaveName)
        vbox.addWidget(self.saveButton)
        vbox.addLayout(self.centroid_box)
        vbox.addLayout(self.angle_box)
        vbox.addLayout(self.freq_box)
        # set the vbox layout as the widgets layout
        self.setLayout(vbox)

        # create the video capture thread
        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        # start the thread
        self.thread.start()

    @pyqtSlot(np.ndarray)
    def update_image(self, frame):
        """Updates the image_label with a new opencv image"""
        # qt_img = self.convert_cv_qt(frame)
        # self.image_label.setPixmap(qt_img)
        self.nb_frame += 1
        t_start_treatment = time.time()
        [contours, centroids] = self.compute_centroids(frame, self.thresh)
        self.centroids = centroids
        # print(self.centroids[0][0])
        self.centroid_left_text_2.setText(str(self.centroids[0][0]))
        self.centroid_left_text_3.setText(str(self.centroids[0][1]))
        self.centroid_right_text_2.setText(str(self.centroids[1][0]))
        self.centroid_right_text_3.setText(str(self.centroids[1][1]))
        # print(self.centroids)
        if self.calib_done:
            # self.angles[0][0] = (centroids[0][0] - self.b_leftH) / self.a_leftH
            # self.angles[0][1] = (centroids[0][1] - self.b_leftV) / self.a_leftV
            # self.angles[1][0] = (centroids[1][0] - self.b_rightH) / self.a_rightH
            # self.angles[1][1] = (centroids[1][1] - self.b_rightV) / self.a_rightV
            x_l = centroids[0][0]
            y_l = centroids[0][1]
            x_r = centroids[1][0]
            y_r = centroids[1][1]
            c_l = self.regression_coefs_left
            c_r = self.regression_coefs_right
            self.angles[0][0] = c_l[0][0] + c_l[1][0] * x_l + c_l[2][0] * y_l + c_l[3][0] * x_l**2 + c_l[4][0] * x_l * y_l + c_l[5][0] * y_l**2
            self.angles[0][1] = c_l[0][1] + c_l[1][1] * x_l + c_l[2][1] * y_l + c_l[3][1] * x_l**2 + c_l[4][1] * x_l * y_l + c_l[5][1] * y_l**2
            self.angles[1][0] = c_r[0][0] + c_r[1][0] * x_r + c_r[2][0] * y_r + c_r[3][0] * x_r**2 + c_r[4][0] * x_r * y_r + c_r[5][0] * y_r**2
            self.angles[1][1] = c_r[0][1] + c_r[1][1] * x_r + c_r[2][1] * y_r + c_r[3][1] * x_r**2 + c_r[4][1] * x_r * y_r + c_r[5][1] * y_r**2
            # print(self.angles)
            self.angle_left_text_2.setText(str(self.angles[0][0]))
            self.angle_left_text_3.setText(str(self.angles[0][1]))
            self.angle_right_text_2.setText(str(self.angles[1][0]))
            self.angle_right_text_3.setText(str(self.angles[1][1]))


        qt_img = self.convert_cv_qt(frame, contours)
        self.image_label.setPixmap(qt_img)
        if self.save_im == True:
            cv2.imwrite(self.image_save_name + '.png', frame)
            self.save_im = not(self.save_im)

        # Mesure de la fréquence de fonctionnement
        t_stop_treatment = time.time()
        if self.nb_frame % 50 == 1:
            self.working_freq = 1 / (t_stop_treatment - t_start_treatment)
            self.freq_label.setText(str(self.working_freq))

        # time.sleep(1e-3)  # Slow down the program to allow the computation of centroids to be complete


    def convert_cv_qt(self, frame, contours):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # rgb_image = frame
        h, w, ch = rgb_image.shape
        rgb_image[:,:,1] = np.zeros((h,w))
        rgb_image[:,:,2] = np.zeros((h,w))
        contours = contours[0]
        contours_image = np.zeros((w, h))
        if len(contours) > 0:
            contours = np.squeeze(contours)
            contours_image[contours[:, 0], contours[:, 1]] = 255
            # rgb_image[contours[:,0], contours[:,1], 1] = 255
            rgb_image[:, :, 1] = np.rot90(contours_image)[::-1]
            # rgb_image[:,:,1] = rgb_image[:,:,1][::-1]

        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.display_width, self.display_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)

    def value_thresh_changed(self, i):
        """
        Update the threshold value with the mouse
        """
        self.thresh = i
    def value_thresh_changed_str(self, s):
        """
        Update the threshold value by typing directly a value
        """
        self.thresh = int(s)

    def image_save_name_changed(self, new_image_name):
        print(new_image_name)
        self.image_save_name = new_image_name

    # def centroids_changed(self, centroids):
    #     self.centroid_left_text_2 = str(centroids[0][0])
    #     self.centroid_left_text_3 = str(centroids[0][1])
    #     self.centroid_right_text_2 = str(centroids[1][0])
    #     self.centroid_right_text_3 = str(centroids[1][1])

    def save_button_pushed(self):
        print('Enregistrement d\'images')
        self.save_im = True
        # im = self.convert_cv_qt(self.update_image)
        # print(type(im))
        # cv2.imwrite('test.png', im)
        # name = QFileDialog.getSaveFileName(self, 'Save File',"image.png")
        # name.setNameFilters(["*.py"])
        # name.selectNameFilter("Python Files (*.py)")
        # file = open(name, 'w')
    def calib_button_pushed(self):
        if  self.calib_step <= 4:
            if self.calib_step < 4:
                self.current_angleH = anglesH_calib[self.calib_step+1]
                self.current_angleV = anglesV_calib[self.calib_step+1]
                print(self.current_angleH)
                print(self.current_angleV)
            self.current_angleH_label.setText(str(self.current_angleH))
            self.current_angleV_label.setText(str(self.current_angleV))
            self.calibH[0, self.calib_step] = self.centroids[0][0]
            self.calibH[1, self.calib_step] = self.centroids[1][0]
            self.calibV[0, self.calib_step] = self.centroids[0][1]
            self.calibV[1, self.calib_step] = self.centroids[1][1]
            print(self.calibH)
            print(self.calibV)
            self.calib_step += 1
            print("Calibration step realized:", self.calib_step)
            if self.calib_step < 5:
                self.calib_step_label.setText(str(self.calib_step))
        elif self.calib_step == 5:
            if not self.calib_done:
                self.calib_done = True
                self.calib_step += 1
                print(anglesH_calib.reshape(1,-1)[0])
                print(anglesH_calib.reshape(1,-1).shape)
                print(self.calibH[0,:].shape)

                self.angles_calib = np.array([anglesH_calib, anglesV_calib]).transpose()
                self.regression_coefs_left = calibration.polynomial_regression(self.calibH[:, 0], self.calibV[:, 0], self.angles_calib, deg=3)
                self.regression_coefs_right = calibration.polynomial_regression(self.calibH[:, 1], self.calibV[:, 1], self.angles_calib, deg=3)
                # self.reg_leftH.fit(anglesH_calib.reshape(-1,1), self.calibH[0,:])
                # print(anglesH_calib.reshape(-1,1))
                # print(self.calibH[0,:])
                # self.reg_rightH.fit(anglesH_calib.reshape(-1,1), self.calibH[1,:])
                # self.reg_leftV.fit(anglesV_calib.reshape(-1,1), self.calibV[0,:])
                # self.reg_rightV.fit(anglesV_calib.reshape(-1,1), self.calibV[1,:])
                # self.a_leftH = self.reg_leftH.coef_
                # self.b_leftH = self.reg_leftH.intercept_
                # self.a_rightH = self.reg_rightH.coef_
                # self.b_rightH = self.reg_leftH.intercept_
                # self.a_leftV = self.reg_leftV.coef_
                # self.b_leftV = self.reg_leftH.intercept_
                # self.a_rightV = self.reg_rightV.coef_
                # self.b_rightV = self.reg_leftH.intercept_
                # print(self.a_leftH)
                # print(self.b_leftV)
                # print(self.a_rightH)
                # print(self.b_rightV)
                # print(self.a_leftH)
                # print(self.b_leftV)
                # print(self.a_rightH)
                # print(self.b_rightV)
                # plt.plot(anglesH_calib, self.calibH[0,:], 'r*')
                # plt.plot(anglesH_calib, self.a_leftH[0] * anglesH_calib + self.b_leftH, 'r-')
                # plt.show()
            print('Calibration complete !')
            self.calib_step_label.setText('Calibration complete !')



    def compute_centroids(self, im_cv, thresh=20):
        # Ligne ci-dessous inutile, l'image est déjà en grayscale
        # im_gray = cv2.cvtColor(im_cv, cv2.COLOR_BGR2GRAY)
        im_gray = im_cv
        ret, im_thr = cv2.threshold(im_gray, thresh, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(im_thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours_sorted = bubble_sort_lists(contours)
        # print(type(contours_sorted))
        if contours_sorted is None:
            # print(contours_sorted is None)
            c1 = []
            cX1 = 0
            cY1 = 0
        else:
            c1 = contours_sorted[0]
            cX1, cY1 = centroid_contour(c1)
        # Recherche du deuxième plus grand contour suffisamment séparé pour être
        # le second spot
        c2 = []
        cX2 = 0
        cY2 = 0

        second_spot_exists = False
        if not(contours_sorted is None) and (len(contours_sorted) > 1):
            i = 2
            c_temp = contours_sorted[1]
            cX_temp, cY_temp = centroid_contour(c_temp)
            # Calcul de la distance entre les deux barycentres
            # il faut cette distance > 1/10 de la largeur de l'image pour être bien
            # sur deux spots séparés
            d_x = np.abs(cX1 - cX_temp)
            d_y = np.abs(cY1 - cY_temp)
            d_between_centroids = np.sqrt(d_x * d_x + d_y * d_y)
            second_spot_exists = (d_between_centroids > 0.05 * im_gray.shape[0])  # Comparaison à la largeur de l'image
            while not(second_spot_exists) and (i < len(contours_sorted)):
                c_temp = contours_sorted[i]
                cX_temp, cY_temp = centroid_contour(c_temp)
                d_x = np.abs(cX1 - cX_temp)
                d_y = np.abs(cY1 - cY_temp)
                d_between_centroids = np.sqrt(d_x * d_x + d_y * d_y)
                second_spot_exists = (d_between_centroids > 0.05 * im_gray.shape[0])
                i += 1

        if second_spot_exists:
            c2 = c_temp
            cX2 = cX_temp
            cY2 = cY_temp

        contours = [c1, c2]  # Recréation de la liste avec les 2 contours

        # Sauvegarde des barycentres
        if len(c2) == 0:  # Si il n'y a qu'un contour
            # if angles_list[k] < 0:
            #     centroids_left = [cX2, cY2]
            #     centroids_right = [cX1, cY1]
            # if angles_list[k] > 0:
            #     centroids_left = [cX1, cY1]
            #     centroids_right = [cX2, cY2]
            centroids_left = [cX1, cY1]
            centroids_right = [cX2, cY2]
        else:
            if cX1 < cX2:
                centroids_left = [cX1, cY1]
                centroids_right = [cX2, cY2]
            else:
                centroids_left = [cX2, cY2]
                centroids_right = [cX1, cY1]

        centroids = np.array([centroids_left, centroids_right])
        return (contours, centroids)

        # def update_freq_table(self):
        #     if np.all(self.freq_table != 0):
        #         self.working_freq = np.mean(self.freq_table)
        #         self.freq_table[:]=0
        #     else:
        #         pass


if __name__=="__main__":
    app = QApplication(sys.argv)
    a = App()
    a.show()
    sys.exit(app.exec_())
