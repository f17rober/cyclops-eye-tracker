from __future__ import print_function
from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QHBoxLayout, QVBoxLayout, QLineEdit, QPushButton, QFileDialog
from PyQt5.QtGui import QPixmap
import sys
import time
import copy
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np
import uvc

dev_list = uvc.device_list()

def bubble_sort_lists(li_origin):
    """
    Sort the list of lists by length of its list elements.

    Parameters
    ----------
    li_origin : List of lists

    Returns
    -------
    list_sorted : The list sorted by the size of each of its elements. The
    lists with biggest lengths are firsts. Delete the contours of two pixels or
    less.

    """
    li = copy.deepcopy(li_origin)
    # if li == []:
    #     return []
    for i in range(len(li) - 1, 0, -1):
        for j in range(i):
            if len(li[j]) < len(li[j + 1]):
                temp = li[j + 1]
                li[j + 1] = li[j]
                li[j] = temp
                # li[j + 1], li[j] = li[j], li[j + 1]
    try:
        while len(li[len(li)-1]) <= 2:
            li.pop()
        return li
    except:
        # print(li)
        # import pdb; pdb.set_trace()
        pass

def centroid_contour(c):
    """
    Compute the (non-weighted) centroid of a contour.

    Parameters
    ----------
    c : Open-cv contour.

    Returns
    -------
    cX : X-value of the centroid
    cY : Y-value of the centroid

    """
    M = cv2.moments(c)
    if M["m00"]:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
    else:
        print("Unable to compute the centroid with moments for the\
                        angle of " + ". It is then computed\
                        with a mean of he X-values and Y-values.")
        cX = np.mean(c[1, :, 0])
        cY = np.mean(c[1, :, 1])
    return cX, cY


class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def run(self):
        # capture from web cam
        cap = uvc.Capture(dev_list[1]["uid"])
        cap.frame_mode = (400, 400, 120)
        while True:
            frame = cap.get_frame_robust()
            self.change_pixmap_signal.emit(frame.gray)


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Eye camera")
        self.display_width = 640
        self.display_height = 480
        self.save_im = False
        self.image_save_name = 'image'
        self.nb_frame = 0
        # create the label that holds the image
        self.image_label = QLabel(self)
        self.image_label.resize(self.display_width, self.display_height)
        # create a text label
        self.textLabel = QLabel('Left eye')
        self.centroidsButton = QPushButton('Compute centroids')
        # self.centroidsButton.clicked.connect(self.compute_centroids)
        self.imageSaveName = QLineEdit()
        self.imageSaveName.setText(self.image_save_name)
        self.imageSaveName.textChanged.connect(self.image_save_name_changed)
        self.saveButton = QPushButton('Save Image')
        self.saveButton.clicked.connect(self.save_button_pushed)

        # Widget indiquant les barycentres calculés
        self.centroids = np.zeros((2,2))  # Initialisation des valeurs de barycentre
        self.centroid_left_1 = str(self.centroids[0][0])
        self.centroid_left_2 = str(self.centroids[0][1])
        self.centroid_right_1 = str(self.centroids[1][0])
        self.centroid_right_2 = str(self.centroids[1][1])
        self.centroid_left_text_1 = QLabel('Centroid Left')
        self.centroid_left_text_2 = QLabel(self.centroid_left_1)
        self.centroid_left_text_3 = QLabel(self.centroid_left_2)
        self.centroid_left_box = QVBoxLayout()
        self.centroid_left_box.addWidget(self.centroid_left_text_1)
        self.centroid_left_box.addWidget(self.centroid_left_text_2)
        self.centroid_left_box.addWidget(self.centroid_left_text_3)
        self.centroid_right_text_1 = QLabel('Centroid right')
        self.centroid_right_text_2 = QLabel(self.centroid_right_1)
        self.centroid_right_text_3 = QLabel(self.centroid_right_2)
        self.centroid_right_box = QVBoxLayout()
        self.centroid_right_box.addWidget(self.centroid_right_text_1)
        self.centroid_right_box.addWidget(self.centroid_right_text_2)
        self.centroid_right_box.addWidget(self.centroid_right_text_3)
        self.centroid_box = QHBoxLayout()
        self.centroid_box.addLayout(self.centroid_left_box)
        self.centroid_box.addLayout(self.centroid_right_box)

        # Widget indiquant la fréquence de fonctionnement en temps réel
        self.working_freq = 0
        self.freq_label = QLabel(str(self.working_freq))
        self.freq_box = QHBoxLayout()
        self.freq_box.addWidget(QLabel('Frequency'))
        self.freq_box.addWidget(self.freq_label)
        # create a vertical box layout and add the two labels
        vbox = QVBoxLayout()
        vbox.addWidget(self.image_label)
        vbox.addWidget(self.textLabel)
        # vbox.addWidget(self.centroidsButton)
        vbox.addWidget(self.imageSaveName)
        vbox.addWidget(self.saveButton)
        vbox.addLayout(self.centroid_box)
        vbox.addLayout(self.freq_box)
        # set the vbox layout as the widgets layout
        self.setLayout(vbox)

        # create the video capture thread
        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        # start the thread
        self.thread.start()



    @pyqtSlot(np.ndarray)
    def update_image(self, frame):
        """Updates the image_label with a new opencv image"""
        # qt_img = self.convert_cv_qt(frame)
        # self.image_label.setPixmap(qt_img)
        self.nb_frame += 1
        t_start_treatment = time.time()
        [contours, centroids] = self.compute_centroids(frame, 15)
        self.centroids = centroids
        # print(self.centroids[0][0])
        self.centroid_left_text_2.setText(str(self.centroids[0][0]))
        self.centroid_left_text_3.setText(str(self.centroids[0][1]))
        self.centroid_right_text_2.setText(str(self.centroids[1][0]))
        self.centroid_right_text_3.setText(str(self.centroids[1][1]))
        qt_img = self.convert_cv_qt(frame, contours)
        self.image_label.setPixmap(qt_img)
        if self.save_im == True:
            cv2.imwrite(self.image_save_name + '.png', frame)
            self.save_im = not(self.save_im)

        # Mesure de la fréquence de fonctionnement
        t_stop_treatment = time.time()
        if self.nb_frame % 50 == 1:
            self.working_freq = 1 / (t_stop_treatment - t_start_treatment)
            self.freq_label.setText(str(self.working_freq))


    def convert_cv_qt(self, frame, contours):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # rgb_image = frame
        h, w, ch = rgb_image.shape
        rgb_image[:,:,1] = np.zeros((h,w))
        rgb_image[:,:,2] = np.zeros((h,w))
        contours = contours[0]
        contours_image = np.zeros((w, h))
        if len(contours) > 0:
            contours = np.squeeze(contours)
            contours_image[contours[:, 0], contours[:, 1]] = 255
            # rgb_image[contours[:,0], contours[:,1], 1] = 255
            rgb_image[:, :, 1] = np.rot90(contours_image)[::-1]
            # rgb_image[:,:,1] = rgb_image[:,:,1][::-1]

        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.display_width, self.display_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)

    def image_save_name_changed(self, new_image_name):
        print(new_image_name)
        self.image_save_name = new_image_name

    # def centroids_changed(self, centroids):
    #     self.centroid_left_text_2 = str(centroids[0][0])
    #     self.centroid_left_text_3 = str(centroids[0][1])
    #     self.centroid_right_text_2 = str(centroids[1][0])
    #     self.centroid_right_text_3 = str(centroids[1][1])

    def save_button_pushed(self):
        print('Enregistrement d\'images')
        self.save_im = True
        # im = self.convert_cv_qt(self.update_image)
        # print(type(im))
        # cv2.imwrite('test.png', im)
        # name = QFileDialog.getSaveFileName(self, 'Save File',"image.png")
        # name.setNameFilters(["*.py"])
        # name.selectNameFilter("Python Files (*.py)")
        # file = open(name, 'w')

    def compute_centroids(self, im_cv, thresh=20):
        # Ligne ci-dessous inutile, l'image est déjà en grayscale
        # im_gray = cv2.cvtColor(im_cv, cv2.COLOR_BGR2GRAY)
        im_gray = im_cv
        ret, im_thr = cv2.threshold(im_gray, thresh, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(im_thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours_sorted = bubble_sort_lists(contours)
        # print(type(contours_sorted))
        if contours_sorted is None:
            # print(contours_sorted is None)
            c1 = []
            cX1 = 0
            cY1 = 0
        else:
            c1 = contours_sorted[0]
            cX1, cY1 = centroid_contour(c1)
        # Recherche du deuxième plus grand contour suffisamment séparé pour être
        # le second spot
        c2 = []
        cX2 = 0
        cY2 = 0

        second_spot_exists = False
        if not(contours_sorted is None) and (len(contours_sorted) > 1):
            i = 2
            c_temp = contours_sorted[1]
            cX_temp, cY_temp = centroid_contour(c_temp)
            # Calcul de la distance entre les deux barycentres
            # il faut cette distance > 1/10 de la largeur de l'image pour être bien
            # sur deux spots séparés
            d_x = np.abs(cX1 - cX_temp)
            d_y = np.abs(cY1 - cY_temp)
            d_between_centroids = np.sqrt(d_x * d_x + d_y * d_y)
            second_spot_exists = (d_between_centroids > 0.05 * im_gray.shape[0])  # Comparaison à la largeur de l'image
            while not(second_spot_exists) and (i < len(contours_sorted)):
                c_temp = contours_sorted[i]
                cX_temp, cY_temp = centroid_contour(c_temp)
                d_x = np.abs(cX1 - cX_temp)
                d_y = np.abs(cY1 - cY_temp)
                d_between_centroids = np.sqrt(d_x * d_x + d_y * d_y)
                second_spot_exists = (d_between_centroids > 0.05 * im_gray.shape[0])
                i += 1

        if second_spot_exists:
            c2 = c_temp
            cX2 = cX_temp
            cY2 = cY_temp

        contours = [c1, c2]  # Recréation de la liste avec les 2 contours

        # Sauvegarde des barycentres
        if len(c2) == 0:  # Si il n'y a qu'un contour
            # if angles_list[k] < 0:
            #     centroids_left = [cX2, cY2]
            #     centroids_right = [cX1, cY1]
            # if angles_list[k] > 0:
            #     centroids_left = [cX1, cY1]
            #     centroids_right = [cX2, cY2]
            centroids_left = [cX1, cY1]
            centroids_right = [cX2, cY2]
        else:
            if cX1 < cX2:
                centroids_left = [cX1, cY1]
                centroids_right = [cX2, cY2]
            else:
                centroids_left = [cX2, cY2]
                centroids_right = [cX1, cY1]

        centroids = np.array([centroids_left, centroids_right])
        return (contours, centroids)

        # def update_freq_table(self):
        #     if np.all(self.freq_table != 0):
        #         self.working_freq = np.mean(self.freq_table)
        #         self.freq_table[:]=0
        #     else:
        #         pass


if __name__=="__main__":
    app = QApplication(sys.argv)
    a = App()
    a.show()
    sys.exit(app.exec_())
