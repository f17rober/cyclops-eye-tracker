import numpy as np
import matplotlib.pyplot as plt

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline


def polynomial_regression(x, y, angles_calib, deg):
    """
    Return the coefficients of a polynomial regression of degree deg on x and y.
    x: x-positions of the spot (np-array of dim n_calib_steps)
    x: y-positions of the spot (np-array of dim n_calib_steps)
    angles_calib: H and V angles of calibration (np-array of dim n_calib_steps*2)
    deg: degree of the regression's polynome (int)

    RETURNS
    coefs: coefficients for the two polynomes (np-array of dim n_coefs*2)
    """
    # Preprocessing of the data
    measured_values = np.array([x, y]).transpose()
    angles_horizontal = angles_calib[0, :]
    angles_vertical = angles_calib[1, :]

    # Creation of the models
    model_horizontal = Pipeline([('poly', PolynomialFeatures(degree = deg, interaction_only=False)),
                        ('linear', LinearRegression(fit_intercept=False))])
    model_vertical = Pipeline([('poly', PolynomialFeatures(degree = deg, interaction_only=False)),
                        ('linear', LinearRegression(fit_intercept=False))])
    # Fitting of the models and return of the results
    model_horizontal = model_horizontal.fit(measured_values, angles_horizontal)
    coefs_horizontal = model_horizontal.named_steps['linear'].coef_
    print(coefs_horizontal)
    model_vertical = model_vertical.fit(measured_values, angles_vertical)
    coefs_vertical = model_vertical.named_steps['linear'].coef_
    print(coefs_vertical)

    return np.array([coefs_horizontal, coefs_vertical]).transpose()


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    x = np.array([-10,0,10])
    y = np.array([4,5,6])
    angles_horizontal = np.array([-8,8, 93])
    angles_vertical = np.array([10, 11, 12])
    angles = np.array([angles_horizontal, angles_vertical])

    coefs = polynomial_regression(x, y, angles, 3)
    import pdb; pdb.set_trace()
    coefs_horizontal = coefs[:,0]
    x_plot = np.linspace(-10, 10, 100)
    y_plot = x_plot
    angle_horizontal_reg = coefs_horizontal[0] + coefs_horizontal[1] * x_plot + coefs_horizontal[2] * y_plot + coefs_horizontal[3] * x_plot**2 + coefs_horizontal[4] * x_plot*y_plot + coefs_horizontal[5] * y_plot**2 + coefs_horizontal[6] * x_plot**3 + coefs_horizontal[7] * x_plot**2 * y_plot + coefs_horizontal[8] * x_plot * y_plot**2 + coefs_horizontal[9] * y_plot**3
    plt.figure()
    plt.scatter(x, angles_horizontal)
    plt.plot(x_plot, angle_horizontal_reg)
    plt.show()
